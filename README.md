<div align="center">
  <h1><code>golem</code></h1>

  <p>
    <strong>Customizable cellular automaton simulator.</strong>
  </p>

  <p>
    <a href="https://app.netlify.com/sites/teds/deploys"><img alt="Netlify deploy status" src="https://api.netlify.com/api/v1/badges/25b4d6b3-743c-4d32-9f12-64fa5edf0257/deploy-status"></a>
  </p>
</div>

## About

Started from the Rust + Wasm [game of life tutorial](https://rustwasm.github.io/docs/book/introduction.html).
