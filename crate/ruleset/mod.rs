//! Rulesets for cellular automata.

mod bs;
mod bsc;

pub use bs::BS;
pub use bsc::BSC;
