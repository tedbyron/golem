import { useState } from 'react'

import GolemStage from '../components/golem-stage'
import GolemStats from '../components/golem-stats'
import GolemOptions from '../components/golem-options'
import Layout from '../components/layout'

// generate colors for first 25 cell states
const colors = [0x212121, 0xffd600]
for (let i = 0; i < 23; i += 1) {
  colors.push(parseInt(Math.floor(Math.random() * 16777215).toString(16), 16))
}

const IndexPage = () => {
  const [run, setRun] = useState(true)
  const [cellSize] = useState(5)
  const [rules] = useState({ // TODO
    birth: [3],
    survival: [2, 3],
    generation: 2
  })

  return (
    <Layout>
      <section>
        <div className='golem-heading-wrapper'>
          <h1 className='golem-heading'>Golem</h1>
        </div>

        {typeof window !== 'undefined' && (
          <GolemStage
            cellSize={cellSize}
            rules={rules}
            run={run}
            colors={colors}
          />
        )}

        <div className='golem-control'>
          <GolemStats />
          <GolemOptions
            handleStartStop={() => setRun(!run)}
            handleStep={() => {}} // TODO
          />
        </div>
      </section>
    </Layout>
  )
}

export default IndexPage
